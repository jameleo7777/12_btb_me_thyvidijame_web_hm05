import React from 'react'
import CenterBar from './CenterBar'
import LeftNavBar from './LeftNavBar'
import RightNavBar from './RightNavBar'

const HomePage = ({passData, passNewData}) => {
  return (
    <div class="grid grid-cols-12 h-screen w-screen ">
      <div class="col-span-1"> <RightNavBar/> </div>
      <div class="col-span-8 bg-white pl-2"> 
          <CenterBar passData = {passData} passNewData = {passNewData} />
       </div>
      <div class="col-span-3 "><LeftNavBar/> </div>
    </div>
  )
}

export default HomePage
