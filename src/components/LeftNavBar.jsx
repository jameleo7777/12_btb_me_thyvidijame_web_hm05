import React from "react";
import Notification from "../asset/notification.png"
import Comment from "../asset/comment.png"
import Lachlan from "../asset/lachlan.jpg"
import Raamin from "../asset/raamin.jpg"
import Nonanamesontheway from "../asset/nonamesontheway.jpg"
import Christina from "../asset/christina.jpg"


export default function LeftNavBar() {
  return (
    <div id="Camping" className="h-screen">
    <div className="flex pt-8 ml-32">
      <div className="flex justify-end items-center ml-6 ">
        {/* notification logo */}

        <img
          src={Notification}
          alt="notification"
          className="w-8 h-8 "
        />
      </div>

      <div className="flex justify-center items-center ml-6">
        {/* comment logo */}

        <img 
        src={Comment} 
        alt="comment" className="w-8 h-8 " />
      </div>

      <div className="flex justify-center items-center ml-6 ">
        {/* lachlan picture */}

        <img
          src={Lachlan}
          // src ={require("../asset/christina.jpg")}
          alt="lachlan"
          className="w-10 h-10 rounded-full"
        />
      </div>
    </div>

    <div>
      <button className="bg-[#EB455F] p-2.5 ml-32 my-5 rounded-xl w-[175px]  font-bold text-black">
        My amazing trip
      </button>
    </div>

    <div className="mx-4 text-3xl text-white font-normal">
      I Like laying down on the <br /> sand and looking at the <br /> moon.
    </div>

    <div className="mt-12 mx-4 text-lg font-medium text-white">27 people going to this trip </div>

    <div className="flex pt-8">
      <div className="flex justify-center items-center ml-4 ">
        {/* lachlan picture */}

        <img
          src={Lachlan}
          alt="lachlan"
          className="w-12 h-12 rounded-full"
        />
      </div>

      <div className="flex justify-center items-center ml-4 ">
        {/* raamin picture */}

        <img
          src={Raamin}
          alt="raamin"
          className="w-12 h-12 rounded-full border-2 border-white"
        />
      </div>

      <div className="flex justify-center items-center ml-4 ">
        {/* nonamesontheway picture */}

        <img
          src={Nonanamesontheway}
          alt="nonamesontheway"
          className="w-12 h-12 rounded-full border-2 border-red-600"
        />
      </div>

      <div className="flex justify-center items-center ml-4 ">
        {/* christina picture */}

        <img
          src={Christina}
          alt="christina"
          className="w-12 h-12 rounded-full border-2 border-white"
        />
      </div>

      <div className="flex justify-center items-center ml-4 ">
        {/* 23+ */}

        <div className="w-12 h-12 text-center items-center rounded-full border-dashed border-2 bg-red-100 border-red-400">
          <h2 className="pt-2.5">23+</h2>
        </div>
      </div>
    </div>
  </div>
  )
}
