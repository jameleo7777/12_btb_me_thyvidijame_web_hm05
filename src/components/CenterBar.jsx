import React, { useState } from "react";
import FormPage from "./FormPage";

function CenterBar({ passData, passNewData }) {
  const [newData, setNewData] = useState([]);

  const changBTN = (id) => {
    passData.map((passData) => {
      if (passData.id === id) {
        if (passData.status === `beach`) {
          passData.status = `forest`;
        } else if (passData.status === `forest`) {
          passData.status = `mountain`;
        } else {
          passData.status = `beach`;
        }
      }
    });

    passNewData([...passData]);
  };

  return (
    <div>
      <div>
        <div className="h-screen ">
          <div className="flex justify-between pt-20">
            <div className="text-3xl ml-2 text-black font-bold ">
              {" "}
              Good Evening Team!{" "}
            </div>
            <div>
              <label htmlFor="my-modal-3" className="btn mr-6 text-white ">
                ADD NEW TRIP
              </label>

              <input type="checkbox" id="my-modal-3" className="modal-toggle" />
              <div className="modal">
                <div className=" relative">
                  <label
                    htmlFor="my-modal-3"
                    className="btn btn-sm btn-circle  absolute right-2 top-2 bg-red-600 hover:bg-red-600"
                  >
                    ✕
                  </label>
                  <FormPage passData={passData} passNewData={passNewData} />
                </div>
              </div>
            </div>
          </div>

          {/* Loop Card */}
          <div className="ml-2 mt-6 flex flex-wrap justify-start gap-x-4 gap-y-4">
            {passData.map((item) => (
              <div className="card w-80 bg-[#2B3467] text-white ">
                <div className="card-body text-start">
                  <h2 className="card-title text-start uppercase ">
                    {item.title === undefined ? "null" : item.title}
                  </h2>
                  <p className="justify-start text-justify line-clamp-3">
                    {item.description === undefined ? "null" : item.description}
                  </p>
                  <p className="mt-2">
                    People Going <br />{" "}
                    <span className="text-2xl font-bold ">
                      {item.peopleGoing === undefined
                        ? "null"
                        : item.peopleGoing}{" "}
                    </span>
                  </p>
                  <div className="card-actions justify-center">
                    <button
                      onClick={() => changBTN(item.id)}
                      className={`${
                        item.status === `mountain`
                          ? "bg-[#537FE7] w-[100px] p-2.5 rounded-lg uppercase font-bold"
                          : item.status === `forest`
                          ? "bg-[#3E6D9C] w-[100px] p-2.5  rounded-lg uppercase font-bold"
                          : "bg-[#678983]  w-[100px] p-2.5  rounded-lg uppercase font-bold"
                      }`}
                    >
                      {item.status === undefined ? "null" : item.status}
                    </button>

                    {/* READ DETAIL */}
                    <label
                      htmlFor="detail"
                      className=" text-white  w-[125px] p-2.5  rounded-lg uppercase font-bold bg-orange-500 text-center ml-4 cursor-pointer"
                      onClick={() => setNewData(item)}
                    >
                      READ DETAIL
                    </label>
                    <input
                      type="checkbox"
                      id="detail"
                      className="modal-toggle"
                    />
                    <div className="modal">
                      <div className="modal-box relative " key={newData.id}>
                        <label
                          htmlFor="detail"
                          className="btn btn-sm btn-circle absolute right-2 top-2 bg-red-600 hover:bg-red-600"
                        >
                          ✕
                        </label>
                        <h3 className="text-lg font-bold text-black uppercase">
                          {newData.title === undefined ? "null" : newData.title} 
                        </h3>
                        <p className="py-4 text-black">
                          { newData.description === undefined ? "null" : newData.description}
                        </p>
                        <p className="text-black">
                          Around{" "}
                          <span className="text-blue-500 font-bold text-xl">
                            {newData.peopleGoing === undefined ? "null" : newData.peopleGoing}
                          </span>{" "}
                          people going there{" "}
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
}

export default CenterBar;
