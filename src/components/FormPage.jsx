import React, { useState } from "react";

const FormPage = ({passData, passNewData}) => {
    const [newData,setNewData] = useState([]);  

    const formHandler = (event) =>{
      setNewData({ ...newData,
        [event.target.name] : event.target.value
        
      })
    }


    const formSubmit = (event) =>{
      event.preventDefault();
      passNewData([...passData, {id: passData.length +1, ...newData}])
      event.target.reset();
      setNewData([]);

    }
  
  return (
    <div>
      <div className=" bg-white m-auto p-6 rounded-xl">
          <form onSubmit={formSubmit} >
          <label className="text-lg text-black font-bold">Title</label>
          <br />    
          <input
            onChange={formHandler}
            name="title"
            type="text"
            placeholder="Sihaknou Ville"
            className="input input-bordered input-success w-full  bg-white text-black font-bold"
          />
          <br />
          <br />
          <label className="text-lg text-black font-bold ">Description</label>
          <br />
          <input
            onChange={formHandler}
            name="description"
            type="text"
            placeholder="Happy place with beautiful beach"
            className="input input-bordered input-success w-full bg-white text-black font-bold"
          />
          <br />
          <br />
          <label className="text-lg text-black font-bold ">People Going</label>
          <br />
          <input
            onChange={formHandler}
            name="peopleGoing"
            type="number"
            placeholder="3200"
            className="input input-bordered input-success w-full bg-white text-black font-bold"
          />
          <br />
          <br />
          <label className="text-lg text-black font-bold ">
            Type of Adventure
          </label>
          <select className="select select-success w-full bg-white text-black"   name="status" onChange={formHandler}>
            <option disabled  selected >
              ---- Choose any option ----
            </option>
            <option value="beach" >Beach</option>
            <option value="forest">Forest</option>
            <option value="mountain">Mountain</option>
          </select>{" "}
          <br />
          <br />
          <button  className="w-[125px] font-bold bg-green-500 p-2.5 rounded-lg cursor-auto" >
            <label htmlFor="my-modal-3"> SUBMIT </label>
         </button>
        </form>
        
      </div>
    </div>
  );
};

export default FormPage;
