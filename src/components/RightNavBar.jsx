import React from "react";
import Category from "../asset/category_icon.png"
import Cube from "../asset/cube.png"
import List from "../asset/list.png"
import Messenger from "../asset/messenger.png"
import Success from "../asset/success.png"
import Security from "../asset/security.png"
import Plus from "../asset/plus.png"
import User from "../asset/users.png"
import Lachlan from "../asset/lachlan.jpg"
import Raamin from "../asset/raamin.jpg"
import Nonanamesontheway from "../asset/nonamesontheway.jpg"
import defaultList from "../asset/defaultList.png"

export default function RightNavBar() {
  return (
    <div className="h-screen bg-[#BAD7E9]">
      <div className="flex justify-center items-center ">
        
        {/* category logo */}

        <img
          src={Category}
          alt="category"
          className="w-7 h-7 mt-8 "
        />
      </div>

      <div className="flex justify-center items-center ">

        {/* cube logo */}

        <img 
          src={Cube}
          alt="cube"
          className="w-7 h-7 mt-10 " />
      </div>

      <div className="flex justify-center items-center ">

        {/* list logo */}

        <img 
          src={List}
          alt="list" 
          className="w-7 h-7 mt-5" 
          />
      </div>

      {/* messenger logo */}

      <div className="flex justify-center items-center ">
        <img
          src={Messenger}
          alt="messenger"
          className="w-7 h-7 mt-5"
        />
      </div>
      
      <div className="flex justify-center items-center ">

      {/* Defaultlist logo */}  

        <img 
        src={defaultList}
        alt="defaultList" 
        className="w-7 h-7 mt-5" 
        />
      </div>

      <div className="flex justify-center items-center ">
        
        {/* success logo */}  

        <img 
        src={Success} 
        alt="success" 
        className="w-7 h-7 mt-10 " 
        />
      </div>

      <div className="flex justify-center items-center ">

          {/* security logo */} 

        <img 
        src={Security}
        alt="security" 
        className="w-7 h-7 mt-5" 
        />
      </div>

      <div className="flex justify-center items-center ">

        {/* users logo */} 
        
        <img 
        src={User}
        alt="users" 
        className="w-7 h-7 mt-5" 
        />
      </div>

      <div className="flex justify-center items-center ">

          {/* lachlan picture */} 

        <img
          src={Lachlan}
          alt="lachlan"
          className="w-8 h-8 rounded-full mt-10"
        />
      </div>

      <div className="flex justify-center items-center ">

        {/* raamin picture */} 

        <img
          src={Raamin}
          alt="raamin"
          className="w-8 h-8 rounded-full mt-5"
        />
      </div>

      <div className="flex justify-center items-center ">

        {/* nonamesontheway picture */} 

        <img
          src={Nonanamesontheway}
          alt="nonamesontheway"
          className="w-8 h-8 rounded-full mt-5 "
        />
      </div>

      <div className="flex justify-center items-center ">

        {/* plus logo */} 

        <img
          src={Plus}
          alt="plus"
          className="w-8 h-8 rounded-full object-contain mt-12 "
        />
      </div>
    </div>
  )
}
